from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
from django.template import loader
from django.contrib.auth import logout
from .models import Contenido, Comentario

# Create your views here.

ENVIAR_CONTENIDO = "Enviar Contenido"
ENVIAR_COMENTARIO = "Enviar Comentario"

@csrf_exempt
def get_content(request, llave):
    if request.method == "PUT":
        valor = request.body.decode('utf-8')
    elif request.method == "POST":
        action = request.POST('action')  
        if action == ENVIAR_CONTENIDO:
            valor = request.POST('valor')  
        elif action == ENVIAR_COMENTARIO:
            c = get_object_or_404(Contenido, clave=llave)
            titulo = request.POST('titulo')  
            cuerpo = request.POST('cuerpo')  
            q = Comentario(contenido=c, titulo=titulo, cuerpo=cuerpo, fecha=timezone.now())
            q.save()

    if valor is not None:
        try:
            c = Contenido.objects(clave=llave)
            c.valor = valor
        except Contenido.DoesNotExist:
            c = Contenido(clave=llave, valor=valor)
        c.save()

    contenido = get_object_or_404(Contenido, clave=llave)
    autenticado = request.user.is_authenticated
    usuario = request.user.username
    context = {'contenido': contenido, 'autenticado': autenticado, 'user': usuario}
    return render(request, 'cms_put/content.html', context)


def index(request):
    content_list = Contenido.objects.all()
    autenticado = request.user.is_authenticated
    context = {'content_list': content_list, 'autenticado':autenticado}
    return render(request, 'cms_put/index.html', context)


def loggedIn(request):
    if request.user.is_authenticated:
        logged = "Te has registrado como " + request.user.username + "."
    else:
        logged = "No estas registrado. Porfavor registrate aqui para acceder. <a href='/login'>Login</a>"
    return HttpResponse(logged)


def logout_view(request):
    logout(request)
    return redirect("/cms/")


def iniciar_sesion(request):
    return redirect("/login")
